package hk.quantr.vcdcomponent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestNegative {

	@Test
	public void test() throws FileNotFoundException, IOException {
		BigInteger i = BigInteger.valueOf(0xfffffffffffffff7l);
		System.out.println(i);
		System.out.println(i.toString(16));
		System.out.println(String.valueOf(i.toString(16)));

		BigInteger i2 = new BigInteger("1111111111111111111111111111111111111111111111111111111111110111", 2);
		System.out.println(i2);
		System.out.println(i2.toString(16));
		System.out.println(String.valueOf(i2.toString(16)));

		BigInteger i3 = BigInteger.valueOf(new BigInteger("1111111111111111111111111111111111111111111111111111111111110111", 2).longValue());
		System.out.println(i3);
		System.out.println(i3.toString(16));
		System.out.println(String.valueOf(i3.toString(16)));
	}
}
