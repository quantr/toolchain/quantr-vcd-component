package hk.quantr.vcdcomponent.tree;

import hk.quantr.javalib.CommonLib;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.Wire;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VCDTreeCellRenderer extends DefaultTreeCellRenderer {

	ImageIcon boxIcon = new ImageIcon(getClass().getResource("box.png"));
	ImageIcon arrowJoinIcon = new ImageIcon(getClass().getResource("arrow_join.png"));
	ImageIcon inputIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("input.png")), 15, 15, Image.SCALE_DEFAULT);
	ImageIcon outputIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("output.png")), 15, 15, Image.SCALE_DEFAULT);
//	ImageIcon inputIcon = new ImageIcon(getClass().getResource("input.png"));
//	ImageIcon outputIcon = new ImageIcon(getClass().getResource("output.png"));
	MyPanel panel = new MyPanel();

	public VCDTreeCellRenderer() {
		panel.setPreferredSize(new Dimension(300, 30)); // DO NOT REMOVE, set it to bigger to prevent not enough space
		panel.label.setMaximumSize(new Dimension(15, 15));
		panel.label.setPreferredSize(new Dimension(15, 15));
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
//		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		Object userObject = node.getUserObject();
		if (userObject instanceof CheckBoxNode) {
			CheckBoxNode checkboxNode = (CheckBoxNode) userObject;
			panel.checkbox.setSelected(checkboxNode.selected);

			userObject = checkboxNode.userObject;
			if (userObject instanceof Scope) {
				Scope scope = (Scope) userObject;
				panel.label.setText(scope.name);
				panel.label.setIcon(boxIcon);
			} else if (userObject instanceof Wire) {
				Wire wire = (Wire) userObject;
				panel.label.setText(wire.name + " [" + wire.size + "]");
				if (wire.type == null) {
					panel.label.setIcon(arrowJoinIcon);
				} else if (wire.type.equals("input")) {
					panel.label.setIcon(inputIcon);
				} else if (wire.type.equals("output")) {
					panel.label.setIcon(outputIcon);
				}
			} else if (userObject instanceof String) {
				String str = (String) userObject;
				panel.label.setText(str);
			} else {
				System.out.println(userObject.getClass());
			}
		}
		return panel;
	}
}
