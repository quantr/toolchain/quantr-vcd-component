package hk.quantr.vcdcomponent.tree;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CheckBoxNode {

	public Object userObject;
	public boolean selected;

	public CheckBoxNode(Object userObject, boolean selected) {
		this.userObject = userObject;
		this.selected = selected;
	}
}
