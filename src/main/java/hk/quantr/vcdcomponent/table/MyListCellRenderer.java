package hk.quantr.vcdcomponent.table;

import hk.quantr.vcdcheck.structure.Compare;
import hk.quantr.vcdcheck.structure.Statement;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyListCellRenderer extends JPanel implements ListCellRenderer<Statement> {

	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();

	public MyListCellRenderer() {
//		label2.setOpaque(true);
		setLayout(new BorderLayout());
		add(label1, BorderLayout.WEST);
		add(label2, BorderLayout.CENTER);
		label2.setHorizontalTextPosition(JLabel.LEADING);
		label1.setText("     ");
		label1.setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList list, Statement statement, int index, boolean isSelected, boolean cellHasFocus) {
		Compare compare = statement.compares.get(0);
		label1.setBackground(Color.decode(statement.color));
		label2.setText(compare.toString() + ", " + statement.color);

		if (isSelected) {
			label2.setForeground(list.getSelectionForeground());
			setBackground(list.getSelectionBackground());
		} else {
			label2.setForeground(list.getForeground());
			setBackground(list.getBackground());
		}
		return this;
	}

}
