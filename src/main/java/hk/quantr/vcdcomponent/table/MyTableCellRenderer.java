package hk.quantr.vcdcomponent.table;

import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableCellRenderer extends JLabel implements TableCellRenderer {// extends DefaultTableCellRenderer {

	public ArrayList<Pair<Integer, Integer>> selectedIndex = new ArrayList<>();
	public ArrayList<Pair<Integer, Integer>> searchIndex = new ArrayList<>();
	public ArrayList<Pair<Integer, Integer>> checked = new ArrayList<>();
	public HashMap<Pair<Integer, Integer>, Color> checkedColor = new HashMap<>();
	public ArrayList<Pair<Integer, Integer>> simulatorCorrect = new ArrayList<>();
	public HashMap<Pair<Integer, Integer>, Color> simulatorCorrectColor = new HashMap<>();
	public ArrayList<Pair<Integer, Integer>> simulatorWrong = new ArrayList<>();
	public HashMap<Pair<Integer, Integer>, Color> simulatorWrongColor = new HashMap<>();
	public ArrayList<Pair<Integer, Integer>> checkPoints = new ArrayList<>();
	ImageIcon tickIcon = new ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/tick.png"));
	ImageIcon crossIcon = new ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/cross.png"));

	public MyTableCellRenderer() {
		setOpaque(true);
		setHorizontalAlignment(JLabel.CENTER);
		Font font = UIManager.getFont("Label.font");
		setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
		setBorder(new MatteBorder(0, 0, 0, 1, Color.decode("#bbbbbb")));
		setDoubleBuffered(true);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int colIndex) {
		Color selectBackground = UIManager.getColor("Table.selectionBackground");
//		Color selectBackground = Color.decode("#ddb8ef");
		Color background = UIManager.getColor("Table.background");
		Color background2 = CommonLib.darker(background, 0.95f);
		Color highlightClk = Color.red;// Color.decode("#f7e7ff");
		Color searchBackground = Color.decode("#a9def1");

		if (checked.contains(Pair.of(colIndex, rowIndex))) {
			setBackground(checkedColor.get(Pair.of(colIndex, rowIndex)));
		} else if (searchIndex.contains(Pair.of(colIndex, rowIndex))) {
			setBackground(searchBackground);
		} else {
			if (isSelected || selectedIndex.indexOf(Pair.of(colIndex, rowIndex)) != -1) {
				setBackground(selectBackground);
			} else {
				if (rowIndex % 2 == 0) {
					setBackground(background2);
				} else {
					setBackground(background);
				}
			}
		}
		setIcon(null);
		if (simulatorCorrect.contains(Pair.of(colIndex, rowIndex))) {
			setIcon(tickIcon);
			setBackground(simulatorCorrectColor.get(Pair.of(colIndex, rowIndex)));
		} else if (simulatorWrong.contains(Pair.of(colIndex, rowIndex))) {
			setIcon(crossIcon);
			setBackground(simulatorWrongColor.get(Pair.of(colIndex, rowIndex)));
		}
		if (checkPoints.indexOf(Pair.of(colIndex, rowIndex)) != -1) {
			setBorder(new LineBorder(Color.red, 2));
		} else {
			setBorder(null);
		}

		setText(String.valueOf(value));
		return this;
	}

	/**
	 * Override for performance.
	 */
	@Override
	public void validate() {
		// Does nothing.
	}

	public void setFontSize(int fontSize) {
		setFont(new Font(getFont().getFontName(), getFont().getStyle(), fontSize));
	}

}
