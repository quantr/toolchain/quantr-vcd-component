package hk.quantr.vcdcomponent.table;

import hk.quantr.vcd.datastructure.Wire;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.Enumeration;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.TableColumn;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableHeaderUI extends BasicTableHeaderUI {

	ImageIcon inputIcon = new ImageIcon(getClass().getResource("input.png"));
	ImageIcon outputIcon = new ImageIcon(getClass().getResource("output.png"));
	Color colors[] = {
		Color.decode("#defffc"),
		Color.decode("#e2e4f6"),
		Color.decode("#fcd5ce"),
		Color.decode("#eddcd2"),
		Color.decode("#fff1e6")};

	final int iconWidth = 15;
	final int height = 150;
	public HashMap<String, Object> vcdCheckWireNames;

//	public MyTableHeaderUI() {
//		inputIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("input.png")), iconWidth, iconWidth, BufferedImage.TYPE_INT_ARGB);
//		outputIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("output.png")), iconWidth, iconWidth, BufferedImage.TYPE_INT_ARGB);
//	}
	@Override
	public void paint(Graphics g, JComponent c) {
		Rectangle rect = g.getClipBounds();
//		g.setColor(Color.white);
//		g.fillRect(0, 0, rect.width, rect.height);
		JViewport viewport = (JViewport) header.getTable().getParent();
		VCDTableModel vcdTableModel = (VCDTableModel) header.getTable().getModel();
		Enumeration enumeration = header.getColumnModel().getColumns();
		int x = viewport.getViewRect().x * -1;
		int colIndex = 0;
		int colorIndex = -1;

		// draw module name
		String lastScopeName = null;
		int xAccumulate = 0;
		int lastModuleXStart = 0;
		Font biggerFont = g.getFont().deriveFont(20f);
		g.setFont(biggerFont);
		while (enumeration.hasMoreElements()) {
			TableColumn aColumn = (TableColumn) enumeration.nextElement();
			Wire wire = vcdTableModel.wires.get(colIndex);

			if (!wire.scope.name.equals(lastScopeName)) {
				if (lastScopeName != null) {
					g.setColor(Color.black);
					int textWidth = g.getFontMetrics().stringWidth(lastScopeName);
					g.drawString(lastScopeName, ((xAccumulate - lastModuleXStart) / 2) - viewport.getViewRect().x + lastModuleXStart - (textWidth / 2), 30);
					lastModuleXStart = xAccumulate;
				}
				lastScopeName = wire.scope.name;
				colorIndex++;
			}
			if (x + aColumn.getPreferredWidth() >= 0) {
				g.setColor(colors[colorIndex % colors.length]);
				g.fillRect(x, rect.y, aColumn.getPreferredWidth(), rect.height);
			}
			xAccumulate += aColumn.getPreferredWidth();
			x += aColumn.getPreferredWidth();
			colIndex++;
		}
		if (lastScopeName != null) {
			g.setColor(Color.black);
			g.drawString(lastScopeName, ((xAccumulate - lastModuleXStart) / 2) - viewport.getViewRect().x + lastModuleXStart, 30);
		}
		// end draw module name

		// draw wire
		enumeration = header.getColumnModel().getColumns();
		x = viewport.getViewRect().x * -1;
		colIndex = 0;
		int topPadding = 50;
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
		while (enumeration.hasMoreElements()) {
			TableColumn aColumn = (TableColumn) enumeration.nextElement();
			if (x >= 0) {
				Wire wire = vcdTableModel.wires.get(colIndex);

				Font font = new Font("Soege", Font.BOLD, 14);
				AffineTransform affineTransform = new AffineTransform();
				affineTransform.rotate(Math.toRadians(-45), 0, 0);
				Font rotatedFont = font.deriveFont(affineTransform);
				g2.setFont(rotatedFont);
				g2.setColor(Color.black);
				String wireName = wire.name.replace("(", "[").replace(")", "]");
				g2.drawString(wireName, x + (aColumn.getWidth() / 2), 90 + topPadding - 20);
				if (vcdCheckWireNames!=null && vcdCheckWireNames.get(wireName) != null) {
					g2.setColor(Color.pink);
					int textWidth = g.getFontMetrics().stringWidth(wireName + " ") + 35;
					int cosX = (int) (textWidth * Math.cos(45));
					int sinY = (int) (textWidth * Math.sin(45));
					g2.drawString((String) vcdCheckWireNames.get(wireName), x + cosX + (aColumn.getWidth() / 2), 90 + topPadding - 20 - sinY + 22);
				}
//				int textWidth = g.getFontMetrics().stringWidth(wire.name);

//				AffineTransform affineTransform2 = new AffineTransform();
//				affineTransform2.translate(x + (aColumn.getWidth() / 2) - (iconWidth / 2), 90 + topPadding);
//				affineTransform2.rotate(Math.toRadians(-45), 0, 0);
//				affineTransform2.scale(0.15f, 0.15f);
				if (wire.type != null) {
					if (wire.type.equals("input")) {
						g2.drawImage(inputIcon.getImage(), x + (aColumn.getWidth() / 2) - (iconWidth / 2), height - iconWidth - 5, 15, 15, null);
					} else if (wire.type.equals("output")) {
						g2.drawImage(outputIcon.getImage(), x + (aColumn.getWidth() / 2) - (iconWidth / 2), height - iconWidth - 5, 15, 15, null);
					}
				}
			}

			x += aColumn.getPreferredWidth();
			colIndex++;
		}
		// end draw wire
//		header.getTable().updateUI();
	}

	@Override
	public Dimension getPreferredSize(JComponent c) {
		return new Dimension(500, height);
	}

}
