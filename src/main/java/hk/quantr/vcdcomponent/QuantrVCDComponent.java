package hk.quantr.vcdcomponent;

import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.IntelliJTheme;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import hk.quantr.riscv_simulator.Simulator;
import hk.quantr.vcd.QuantrVCDLibrary;
import hk.quantr.vcdcomponent.tree.VCDTreeCellRenderer;
import hk.quantr.vcdcomponent.tree.CheckBoxNode;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.VCD;
import hk.quantr.vcd.datastructure.Wire;
import hk.quantr.vcdcheck.MyVCDCheckListener;
import hk.quantr.vcdcheck.VCDCheck;
import hk.quantr.vcdcheck.structure.Compare;
import hk.quantr.vcdcheck.structure.Statement;
import hk.quantr.vcdcomponent.table.MyListCellRenderer;
import hk.quantr.vcdcomponent.table.MyTableHeaderUI;
import hk.quantr.vcdcomponent.table.MyTableCellRenderer;
import hk.quantr.vcdcomponent.table.VCDTableModel;
import hk.quantr.vcdcomponent.tree.CheckBoxNodeEditor;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrVCDComponent extends javax.swing.JPanel {

	VCDTableModel vcdTableModel = new VCDTableModel();
	File folder;
	String fileContent;
	File vcdCheckFile;
	MyTableCellRenderer vcdTableCellRenderer = new MyTableCellRenderer();
	public String modulesOrder[];
	public HashMap<String, Integer> selectSetting = new HashMap<>();
	public ArrayList<String> moduleOrder = new ArrayList<>();
	private int lastDividerLocation;
	File file;
	public String vcdStr;
	MyTableHeaderUI myTableHeaderUI = new MyTableHeaderUI();

	HashMap<Integer, Simulator> simulators = new HashMap<>();

	/**
	 * Creates new form QuantrVCDComponent
	 */
	public QuantrVCDComponent() {
		initComponents();

		vcdTable.setShowHorizontalLines(false);
		vcdTable.setShowVerticalLines(false);
		vcdTable.setIntercellSpacing(new Dimension(0, 0));
		vcdTable.getTableHeader().setUI(myTableHeaderUI);
		vcdTable.setDefaultRenderer(Long.class, vcdTableCellRenderer);
		vcdTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int col = vcdTable.columnAtPoint(new Point(e.getPoint().x + vcdTableScrollPane.getHorizontalScrollBar().getValue(), e.getPoint().y));
				Wire wire = vcdTableModel.getColumnWire(col);

				System.out.println("Copied " + wire.scope.name + "." + wire.name);
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(new StringSelection(wire.scope.name + "." + wire.name), null);
			}
		});

		vcdTableScrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				vcdTable.getTableHeader().repaint();
			}
		});

		vcdCheckList.setCellRenderer(new MyListCellRenderer());

		((CardLayout) getLayout()).show(this, "progressPanel");
		selectSetting.put("rom", 0);
		selectSetting.put("pc_reg", 0);
		selectSetting.put("if_id0", 0);
		selectSetting.put("id0", 1);
		selectSetting.put("id_ex0", 1);
		selectSetting.put("ex0", 2);
		selectSetting.put("ex_mem0", 2);
		selectSetting.put("mem0", 3);
		selectSetting.put("mem_wb0", 3);
		selectSetting.put("register0", 4);

		moduleOrder.add("rom");
		moduleOrder.add("pc_reg");
		moduleOrder.add("if_id0");
		moduleOrder.add("id0");
		moduleOrder.add("id_ex0");
		moduleOrder.add("ex0");
		moduleOrder.add("ex_mem0");
		moduleOrder.add("mem0");
		moduleOrder.add("mem_wb0");

		checkButton.setVisible(false);

		// init font combobox
		Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		HashSet<String> temp = new HashSet<>();
		fontComboBox.addItem(new JTable().getFont());
		for (Font font : allFonts) {
			if (!temp.contains(font.getFontName())) {
				fontComboBox.addItem(font);
				temp.add(font.getFontName());
			}
			if (fontComboBox.getItemCount() == 20) {
				break;
			}
		}
		fontComboBox.setRenderer(new JComboBoxFontRenderer());
		fontComboBox.setSelectedItem(new JTable().getFont());
		// end init font combobox

		initUndoManager();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        hexDecbuttonGroup = new javax.swing.ButtonGroup();
        mainPanel = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        fullWireCheckBox = new javax.swing.JCheckBox();
        riseEdgeCheckBox = new javax.swing.JCheckBox();
        highlightRiseEdgeCheckBox = new javax.swing.JCheckBox();
        jToolBar1 = new javax.swing.JToolBar();
        expandAllButton = new javax.swing.JButton();
        expandAllScopesButton = new javax.swing.JButton();
        allButton = new javax.swing.JButton();
        preButton = new javax.swing.JButton();
        noneButton = new javax.swing.JButton();
        selectInOutOnlyCheckBox = new javax.swing.JCheckBox();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jScrollPane5 = new javax.swing.JScrollPane();
        vcdCheckList = new javax.swing.JList<>();
        jPanel5 = new javax.swing.JPanel();
        vcdTableScrollPane = new javax.swing.JScrollPane();
        vcdTable = new javax.swing.JTable();
        jPanel6 = new javax.swing.JToolBar();
        jLabel2 = new javax.swing.JLabel();
        fontSizeTextField = new javax.swing.JTextField();
        fontComboBox = new javax.swing.JComboBox<>();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jLabel1 = new javax.swing.JLabel();
        noOfRowTextField = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        maxScreenButton = new javax.swing.JButton();
        searchTextField = new javax.swing.JTextField();
        exactMatchCheckBox = new javax.swing.JCheckBox();
        selectModuleCheckBox = new javax.swing.JCheckBox();
        selectMultipleCheckBox = new javax.swing.JCheckBox();
        clearButton = new javax.swing.JButton();
        checkButton = new javax.swing.JButton();
        signedCheckBox = new javax.swing.JCheckBox();
        hexRadioButton = new javax.swing.JRadioButton();
        decRadioButton = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jsonEditorPane = new javax.swing.JEditorPane();
        vcdCheckPanel = new javax.swing.JPanel();
        vcdCheckToolBar = new javax.swing.JToolBar();
        refreshVCDCheckButton = new javax.swing.JButton();
        saveVCDCheckFileButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        vcdCheckEditorPane = new javax.swing.JEditorPane();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        vcdCheckLogTextArea = new javax.swing.JTextArea();
        jToolBar2 = new javax.swing.JToolBar();
        openButton = new javax.swing.JButton();
        refreshButton = new javax.swing.JButton();
        settingButton = new javax.swing.JButton();
        marginTextField = new javax.swing.JTextField();
        rowHeightTextField = new javax.swing.JTextField();
        minColWidthButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        lafComboBox = new javax.swing.JComboBox<>();
        progressPanel = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();

        setPreferredSize(new java.awt.Dimension(800, 600));
        setLayout(new java.awt.CardLayout());

        mainPanel.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jSplitPane1.setDividerLocation(300);

        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        jPanel3Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel3.setLayout(jPanel3Layout);

        fullWireCheckBox.setText("Full Wire Width");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(fullWireCheckBox, gridBagConstraints);

        riseEdgeCheckBox.setSelected(true);
        riseEdgeCheckBox.setText("Rise Edge Only");
        riseEdgeCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                riseEdgeCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(riseEdgeCheckBox, gridBagConstraints);

        highlightRiseEdgeCheckBox.setText("Highlight Rise Edge");
        highlightRiseEdgeCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                highlightRiseEdgeCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(highlightRiseEdgeCheckBox, gridBagConstraints);

        jPanel4.add(jPanel3, java.awt.BorderLayout.SOUTH);

        jToolBar1.setRollover(true);

        expandAllButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/folder_go.png"))); // NOI18N
        expandAllButton.setFocusable(false);
        expandAllButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        expandAllButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        expandAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expandAllButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(expandAllButton);

        expandAllScopesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/folder_star.png"))); // NOI18N
        expandAllScopesButton.setFocusable(false);
        expandAllScopesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        expandAllScopesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        expandAllScopesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expandAllScopesButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(expandAllScopesButton);

        allButton.setText("All");
        allButton.setToolTipText("");
        allButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(allButton);

        preButton.setText("Pre");
        preButton.setToolTipText("");
        preButton.setFocusable(false);
        preButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        preButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        preButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(preButton);

        noneButton.setText("None");
        noneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noneButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(noneButton);

        selectInOutOnlyCheckBox.setSelected(true);
        selectInOutOnlyCheckBox.setText("In/Out");
        selectInOutOnlyCheckBox.setFocusable(false);
        selectInOutOnlyCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        selectInOutOnlyCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        selectInOutOnlyCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectInOutOnlyCheckBoxActionPerformed(evt);
            }
        });
        jToolBar1.add(selectInOutOnlyCheckBox);

        jPanel4.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jSplitPane2.setDividerLocation(600);
        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jTree1.setCellRenderer(new VCDTreeCellRenderer());
        jTree1.setRowHeight(22);
        jTree1.setShowsRootHandles(true);
        jTree1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTree1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTree1);

        jSplitPane2.setLeftComponent(jScrollPane2);

        jScrollPane5.setViewportView(vcdCheckList);

        jSplitPane2.setRightComponent(jScrollPane5);

        jPanel4.add(jSplitPane2, java.awt.BorderLayout.CENTER);

        jSplitPane1.setLeftComponent(jPanel4);

        jPanel5.setLayout(new java.awt.BorderLayout());

        vcdTable.setModel(vcdTableModel);
        vcdTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        vcdTable.setRowSelectionAllowed(false);
        vcdTable.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        vcdTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                vcdTableMouseClicked(evt);
            }
        });
        vcdTableScrollPane.setViewportView(vcdTable);

        jPanel5.add(vcdTableScrollPane, java.awt.BorderLayout.CENTER);

        jLabel2.setText("Font");
        jPanel6.add(jLabel2);

        fontSizeTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fontSizeTextField.setText("12");
        fontSizeTextField.setMaximumSize(new java.awt.Dimension(50, 100));
        fontSizeTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fontSizeTextFieldKeyPressed(evt);
            }
        });
        jPanel6.add(fontSizeTextField);

        fontComboBox.setMaximumSize(new java.awt.Dimension(200, 100));
        fontComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontComboBoxActionPerformed(evt);
            }
        });
        jPanel6.add(fontComboBox);
        jPanel6.add(jSeparator2);

        jLabel1.setText("No Of Row");
        jPanel6.add(jLabel1);

        noOfRowTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        noOfRowTextField.setText("1000");
        noOfRowTextField.setMaximumSize(new java.awt.Dimension(70, 26));
        noOfRowTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                noOfRowTextFieldKeyPressed(evt);
            }
        });
        jPanel6.add(noOfRowTextField);
        jPanel6.add(jSeparator3);

        maxScreenButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/arrow_out.png"))); // NOI18N
        maxScreenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxScreenButtonActionPerformed(evt);
            }
        });
        jPanel6.add(maxScreenButton);

        searchTextField.setMaximumSize(new java.awt.Dimension(150, 100));
        searchTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchTextFieldKeyReleased(evt);
            }
        });
        jPanel6.add(searchTextField);

        exactMatchCheckBox.setText("Exact");
        exactMatchCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exactMatchCheckBoxActionPerformed(evt);
            }
        });
        jPanel6.add(exactMatchCheckBox);

        selectModuleCheckBox.setSelected(true);
        selectModuleCheckBox.setText("Module");
        jPanel6.add(selectModuleCheckBox);

        selectMultipleCheckBox.setSelected(true);
        selectMultipleCheckBox.setText("Multiple");
        jPanel6.add(selectMultipleCheckBox);

        clearButton.setText("Clear");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });
        jPanel6.add(clearButton);

        checkButton.setText("Check");
        checkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkButtonActionPerformed(evt);
            }
        });
        jPanel6.add(checkButton);

        signedCheckBox.setSelected(true);
        signedCheckBox.setText("Signed");
        signedCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signedCheckBoxActionPerformed(evt);
            }
        });
        jPanel6.add(signedCheckBox);

        hexDecbuttonGroup.add(hexRadioButton);
        hexRadioButton.setSelected(true);
        hexRadioButton.setText("Hex");
        hexRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hexRadioButtonActionPerformed(evt);
            }
        });
        jPanel6.add(hexRadioButton);

        hexDecbuttonGroup.add(decRadioButton);
        decRadioButton.setText("Dec");
        decRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decRadioButtonActionPerformed(evt);
            }
        });
        jPanel6.add(decRadioButton);

        jPanel5.add(jPanel6, java.awt.BorderLayout.NORTH);

        jSplitPane1.setRightComponent(jPanel5);

        jPanel1.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("VCD", jPanel1);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setViewportView(jsonEditorPane);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Json", jPanel2);

        vcdCheckPanel.setLayout(new java.awt.BorderLayout());

        vcdCheckToolBar.setRollover(true);

        refreshVCDCheckButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/arrow_refresh.png"))); // NOI18N
        refreshVCDCheckButton.setText("Refresh");
        refreshVCDCheckButton.setFocusable(false);
        refreshVCDCheckButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshVCDCheckButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshVCDCheckButtonActionPerformed(evt);
            }
        });
        vcdCheckToolBar.add(refreshVCDCheckButton);

        saveVCDCheckFileButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/disk.png"))); // NOI18N
        saveVCDCheckFileButton.setText("Save");
        saveVCDCheckFileButton.setFocusable(false);
        saveVCDCheckFileButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveVCDCheckFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveVCDCheckFileButtonActionPerformed(evt);
            }
        });
        vcdCheckToolBar.add(saveVCDCheckFileButton);

        vcdCheckPanel.add(vcdCheckToolBar, java.awt.BorderLayout.PAGE_START);

        jScrollPane4.setViewportView(vcdCheckEditorPane);

        vcdCheckPanel.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("VCD Check", vcdCheckPanel);

        jPanel7.setLayout(new java.awt.BorderLayout());

        vcdCheckLogTextArea.setColumns(20);
        vcdCheckLogTextArea.setRows(5);
        jScrollPane3.setViewportView(vcdCheckLogTextArea);

        jPanel7.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("VCD Check Log", jPanel7);

        mainPanel.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        jToolBar2.setRollover(true);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/folder.png"))); // NOI18N
        openButton.setText("Open");
        openButton.setFocusable(false);
        openButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(openButton);

        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/arrow_refresh.png"))); // NOI18N
        refreshButton.setText("Refresh");
        refreshButton.setFocusable(false);
        refreshButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        refreshButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(refreshButton);

        settingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/cog.png"))); // NOI18N
        settingButton.setText("Setting");
        settingButton.setFocusable(false);
        settingButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        settingButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        settingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(settingButton);

        marginTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        marginTextField.setText("10");
        marginTextField.setMaximumSize(new java.awt.Dimension(40, 26));
        marginTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                marginTextFieldKeyPressed(evt);
            }
        });
        jToolBar2.add(marginTextField);

        rowHeightTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        rowHeightTextField.setText("20");
        rowHeightTextField.setMaximumSize(new java.awt.Dimension(40, 26));
        rowHeightTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rowHeightTextFieldKeyPressed(evt);
            }
        });
        jToolBar2.add(rowHeightTextField);

        minColWidthButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/vcdcomponent/table.png"))); // NOI18N
        minColWidthButton.setText("Min Col Width");
        minColWidthButton.setFocusable(false);
        minColWidthButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        minColWidthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minColWidthButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(minColWidthButton);
        jToolBar2.add(jSeparator1);

        lafComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Orange", "Orange Dark", "Material Darker Contrast", "Material Deep Ocean Contrast", "Cyan", "Dark" }));
        lafComboBox.setMaximumSize(new java.awt.Dimension(200, 100));
        lafComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                lafComboBoxItemStateChanged(evt);
            }
        });
        jToolBar2.add(lafComboBox);

        mainPanel.add(jToolBar2, java.awt.BorderLayout.NORTH);

        add(mainPanel, "main");

        progressPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        progressPanel.setLayout(new javax.swing.BoxLayout(progressPanel, javax.swing.BoxLayout.LINE_AXIS));

        jProgressBar1.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jProgressBar1.setStringPainted(true);
        progressPanel.add(jProgressBar1);

        add(progressPanel, "progressPanel");
    }// </editor-fold>//GEN-END:initComponents

    private void jTree1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTree1MouseClicked
		if (jTree1.getSelectionPath() == null) {
			return;
		}
		JProgressBarDialog dialog = new JProgressBarDialog("Updating", true);
		dialog.progressBar.setStringPainted(true);
		dialog.pack();
		dialog.thread = new Thread() {
			public void run() {
				dialog.progressBar.setString("Stat updating");
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree1.getSelectionPath().getLastPathComponent();
				CheckBoxNode selectCheckboxNode = (CheckBoxNode) selectedNode.getUserObject();
				selectAllSubNodes(selectedNode, selectCheckboxNode.selected, dialog.progressBar);
				updateJTreeAfterSelect(dialog.progressBar);
			}
		};
		dialog.setVisible(true);
    }//GEN-LAST:event_jTree1MouseClicked

	public void updateJTreeAfterSelect(JProgressBar progressBar) {
		ArrayList<DefaultMutableTreeNode> selectedNodes = new ArrayList<>();
		getAllSelectedNode((DefaultMutableTreeNode) jTree1.getModel().getRoot(), selectedNodes);
		vcdTableModel.wires.clear();
		for (DefaultMutableTreeNode node : selectedNodes) {
			CheckBoxNode checkboxNode = (CheckBoxNode) node.getUserObject();
			if (checkboxNode.userObject instanceof Wire) {
				if (progressBar != null) {
					progressBar.setString("Updating wire : " + checkboxNode.userObject);
				}
				vcdTableModel.wires.add((Wire) checkboxNode.userObject);
			}
		}
		vcdTableModel.filter();
		vcdTableModel.fireTableStructureChanged();
//		checkButtonActionPerformed(null);
		minColWidthButtonActionPerformed(null);
	}

	private void selectAllSubNodes(DefaultMutableTreeNode selectedNode, boolean b, JProgressBar progressBar) {
		CheckBoxNode selectCheckboxNode = (CheckBoxNode) selectedNode.getUserObject();
		selectCheckboxNode.selected = b;
		if (selectCheckboxNode.userObject instanceof Scope) {
			if (progressBar != null) {
				progressBar.setString("Scope " + selectCheckboxNode.userObject);
			}
			for (int x = 0; x < selectedNode.getChildCount(); x++) {
				DefaultMutableTreeNode s = (DefaultMutableTreeNode) selectedNode.getChildAt(x);
				CheckBoxNode c = (CheckBoxNode) s.getUserObject();
				c.selected = false;

				selectAllSubNodes(s, b, progressBar);
			}
		}
	}

	private void selectAllInOut(DefaultMutableTreeNode selectedNode) {
		CheckBoxNode selectCheckboxNode = (CheckBoxNode) selectedNode.getUserObject();
		if (selectCheckboxNode.userObject instanceof Scope) {
			if (selectCheckboxNode.selected) {
				for (int x = 0; x < selectedNode.getChildCount(); x++) {
					DefaultMutableTreeNode s = (DefaultMutableTreeNode) selectedNode.getChildAt(x);
					CheckBoxNode c = (CheckBoxNode) s.getUserObject();
					if (c.userObject instanceof Wire) {
						Wire wire = (Wire) c.userObject;
						if (selectInOutOnlyCheckBox.isSelected()) {
							if (wire.type != null && (wire.type.equals("input") || wire.type.equals("output"))) {
								c.selected = selectCheckboxNode.selected;
							} else {
								c.selected = false;
							}
						} else {
							c.selected = selectCheckboxNode.selected;
						}
					} else {
						c.selected = selectCheckboxNode.selected;
					}

					selectAllInOut(s);
				}
			}
		}
	}

	private void selectAllScopes(DefaultMutableTreeNode node, String patterns[]) {
		CheckBoxNode selectCheckboxNode = (CheckBoxNode) node.getUserObject();
		if (selectCheckboxNode.userObject instanceof Scope) {
			for (int x = 0; x < node.getChildCount(); x++) {
				DefaultMutableTreeNode s = (DefaultMutableTreeNode) node.getChildAt(x);
				CheckBoxNode c = (CheckBoxNode) s.getUserObject();

				if (c.userObject instanceof Scope) {
					Scope scope = (Scope) c.userObject;
//					System.out.println(scope.name + " = " + Arrays.stream(patterns).anyMatch(scope.name::equals));
					if (Arrays.stream(patterns).anyMatch(scope.name::equals)) {
						c.selected = true;
						selectAllWires(s, true);
						selectAllParentNode((DefaultMutableTreeNode) s.getParent());
					} else {
						c.selected = false;
						selectAllWires(s, false);
					}
				}

				selectAllScopes(s, patterns);
			}
		}
	}

	private void selectAllParentNode(DefaultMutableTreeNode node) {
		if (node != null) {
			CheckBoxNode c = (CheckBoxNode) node.getUserObject();
			c.selected = true;
			selectAllParentNode((DefaultMutableTreeNode) node.getParent());
		}
	}

	private void selectAllWires(DefaultMutableTreeNode node, boolean b) {
		CheckBoxNode selectCheckboxNode = (CheckBoxNode) node.getUserObject();
		if (selectCheckboxNode.userObject instanceof Scope) {
			for (int x = 0; x < node.getChildCount(); x++) {
				DefaultMutableTreeNode s = (DefaultMutableTreeNode) node.getChildAt(x);
				CheckBoxNode c = (CheckBoxNode) s.getUserObject();
				if (c.userObject instanceof Wire) {
					Wire wire = (Wire) c.userObject;
					if (b) {
						if (selectInOutOnlyCheckBox.isSelected()) {
							if (wire.type != null && (wire.type.equals("input") || wire.type.equals("output"))) {
								c.selected = selectCheckboxNode.selected;
							}
						} else {
							c.selected = selectCheckboxNode.selected;
						}
					} else {
						c.selected = false;
					}
				}
			}
		}
	}

    private void expandAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expandAllButtonActionPerformed
		CommonLib.expandAll(jTree1, true);
    }//GEN-LAST:event_expandAllButtonActionPerformed

    private void expandAllScopesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expandAllScopesButtonActionPerformed
		expandAll(jTree1, true);
    }//GEN-LAST:event_expandAllScopesButtonActionPerformed

    private void selectInOutOnlyCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectInOutOnlyCheckBoxActionPerformed
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) jTree1.getModel().getRoot();
		selectAllInOut(root);
    }//GEN-LAST:event_selectInOutOnlyCheckBoxActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
		try {
			loadVCD(file);
		} catch (IOException ex) {
			Logger.getLogger(QuantrVCDComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void highlightRiseEdgeCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_highlightRiseEdgeCheckBoxActionPerformed
		if (highlightRiseEdgeCheckBox.isSelected()) {
//			vcdTableModel.updateClkRiseRowNo();
//			ArrayList<Integer> highlightRowNo = vcdTableModel.getClkRow();
//			System.out.println(highlightRowNo.size());
//			myTableCellRenderer.highlightRowNo = highlightRowNo;
		} else {
//			myTableCellRenderer.highlightRowNo.clear();
		}
//		vcdTableModel.fireTableDataChanged();
		vcdTable.repaint();
    }//GEN-LAST:event_highlightRiseEdgeCheckBoxActionPerformed

    private void riseEdgeCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_riseEdgeCheckBoxActionPerformed
		vcdTableModel.showRiseClockOnly = riseEdgeCheckBox.isSelected();
		vcdTableModel.filter();
		vcdTableModel.fireTableStructureChanged();
		checkButtonActionPerformed(null);
		minColWidthButtonActionPerformed(null);
		highlightRiseEdgeCheckBoxActionPerformed(null);
    }//GEN-LAST:event_riseEdgeCheckBoxActionPerformed

    private void allButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allButtonActionPerformed
		selectTree(true);
    }//GEN-LAST:event_allButtonActionPerformed

    private void noneButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noneButtonActionPerformed
		selectTree(false);
    }//GEN-LAST:event_noneButtonActionPerformed

    private void minColWidthButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minColWidthButtonActionPerformed
		CommonLib.resizeColumnWidth(vcdTable, 0, Integer.parseInt(marginTextField.getText()));
		CommonLib.resizeRowHeight(vcdTable, Integer.parseInt(rowHeightTextField.getText()));
    }//GEN-LAST:event_minColWidthButtonActionPerformed

    private void searchTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTextFieldKeyReleased
		String searchText = searchTextField.getText();
		boolean exactMatch = exactMatchCheckBox.isSelected();
		vcdTableCellRenderer.searchIndex.clear();

		for (int y = 0; y < vcdTableModel.getRowCount(); y++) {
			for (int x = 0; x < vcdTableModel.getColumnCount(); x++) {
				String val = (String) vcdTableModel.getValueAt(y, x);
				if (val != null && searchText != null && searchText.length() > 0) {
					if ((exactMatch && val.equals(searchText)) || (!exactMatch && val.contains(searchText))) {
						vcdTableCellRenderer.searchIndex.add(Pair.of(x, y));
					}
				}
			}
		}

		vcdTableModel.fireTableDataChanged();
    }//GEN-LAST:event_searchTextFieldKeyReleased

    private void exactMatchCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exactMatchCheckBoxActionPerformed
		searchTextFieldKeyReleased(null);
    }//GEN-LAST:event_exactMatchCheckBoxActionPerformed

    private void vcdTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_vcdTableMouseClicked
		int rowNo = vcdTable.getSelectedRow();
		int colNo = vcdTable.getSelectedColumn();

		if (selectModuleCheckBox.isSelected()) {
			Wire wire = vcdTableModel.filterdWires.get(colNo);
			Integer currentLevel = selectSetting.get(wire.scope.name);
			if (currentLevel == null) {
				return;
			}
			if (!selectMultipleCheckBox.isSelected()) {
				vcdTableCellRenderer.selectedIndex.clear();
			}

			for (String scopeName : selectSetting.keySet()) {
				Integer level = selectSetting.get(scopeName);

				for (int x = 0; x < vcdTable.getColumnCount(); x++) {
					if (vcdTableModel.wires.get(x).scope.name.equals(scopeName)) {
						int r = rowNo + (level - currentLevel);
						if (vcdTableCellRenderer.selectedIndex.indexOf(Pair.of(x, r)) == -1) {
							vcdTableCellRenderer.selectedIndex.add(Pair.of(x, r));
						} else {
							vcdTableCellRenderer.selectedIndex.remove(vcdTableCellRenderer.selectedIndex.indexOf(Pair.of(x, r)));
						}
					}
				}
			}
		}

		// highlight checkpoints cells
		vcdTableCellRenderer.checkPoints.clear();
		if (vcdTableModel.checkpoints.get(Pair.of(colNo, rowNo)) != null) {
			vcdTableCellRenderer.checkPoints.addAll(vcdTableModel.checkpoints.get(Pair.of(colNo, rowNo)));
		}
		// end highlight checkpoints cells

		vcdTable.repaint();
    }//GEN-LAST:event_vcdTableMouseClicked

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
		simulators.clear();
		vcdTableCellRenderer.selectedIndex.clear();
		vcdTableCellRenderer.checked.clear();
		vcdTableCellRenderer.checkedColor.clear();
		vcdTableCellRenderer.checkPoints.clear();
		vcdTableCellRenderer.simulatorCorrect.clear();
		vcdTableCellRenderer.simulatorCorrectColor.clear();
		vcdTable.repaint();
    }//GEN-LAST:event_clearButtonActionPerformed

    private void maxScreenButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxScreenButtonActionPerformed
		if (jSplitPane1.getDividerLocation() < 10) {
			jSplitPane1.setDividerLocation(lastDividerLocation);
		} else {
			lastDividerLocation = jSplitPane1.getDividerLocation();
			jSplitPane1.setDividerLocation(0);
		}
    }//GEN-LAST:event_maxScreenButtonActionPerformed

    private void checkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkButtonActionPerformed
		clearButtonActionPerformed(null);
		try {
			if (vcdCheckFile == null || !vcdCheckFile.exists()) {
				return;
			}
			vcdTableCellRenderer.checked.clear();
			vcdTableCellRenderer.checkedColor.clear();
			vcdTableModel.checkpoints.clear();
			MyVCDCheckListener listener = VCDCheck.check(IOUtils.toString(new FileReader(vcdCheckFile)));
			myTableHeaderUI.vcdCheckWireNames = listener.wireNames;
			ArrayList<Statement> statements = listener.statements;

			DefaultListModel listModel = new DefaultListModel();
			for (Statement statement : statements) {
				if (statement.compares.isEmpty()) {
					continue;
				}
				listModel.addElement(statement);

				Compare compare = statement.compares.get(0);
				long value = compare.value;
				int valueOffset = compare.valueOffset;
				for (int y = 0; y < vcdTableModel.getRowCount(); y++) {
					if (compare.type == 0) {
						if (y + valueOffset >= 0) {
							int col1 = vcdTableModel.getColNo(compare.module1, compare.wire1);
							if (col1 == -1) {
								break;
							}
							long v = vcdTableModel.geBigIntegerAt(y + valueOffset, col1).longValue();
							if (v == value) {
								vcdTableCellRenderer.checked.add(Pair.of(col1, y));
								vcdTableCellRenderer.checkedColor.put(Pair.of(col1, y), Color.decode(statement.color));
							}

							// add checkpoint
							addCheckPoint(Pair.of(col1, y), Pair.of(col1, y));
							// end add checkpoint
						}
					} else if (compare.type == 1) {
						if (y + compare.offset1 >= 0 && y + compare.offset2 >= 0) {
							int col1 = vcdTableModel.getColNo(compare.module1, compare.wire1);
							int col2 = vcdTableModel.getColNo(compare.module2, compare.wire2);
							if (col1 == -1 || col2 == -1) {
								break;
							}

							long v1 = vcdTableModel.geBigIntegerAt(y + compare.offset1, col1).longValue();
							long v2 = vcdTableModel.geBigIntegerAt(y + compare.offset2, col2).longValue();
							if (v1 == v2) {
								vcdTableCellRenderer.checked.add(Pair.of(col1, y + compare.offset1));
								vcdTableCellRenderer.checkedColor.put(Pair.of(col1, y + compare.offset1), Color.decode(statement.color));

								vcdTableCellRenderer.checked.add(Pair.of(col2, y + compare.offset2));
								vcdTableCellRenderer.checkedColor.put(Pair.of(col2, y + compare.offset2), Color.decode(statement.color));
							}

							// add checkpoint
							addCheckPoint(Pair.of(col1, y + compare.offset1), Pair.of(col1, y + compare.offset1));
							addCheckPoint(Pair.of(col1, y + compare.offset1), Pair.of(col2, y + compare.offset2));
							addCheckPoint(Pair.of(col2, y + compare.offset2), Pair.of(col1, y + compare.offset1));
							addCheckPoint(Pair.of(col2, y + compare.offset2), Pair.of(col2, y + compare.offset2));
							// end add checkpoint
						}
					} else if (compare.type == 2) {
						if (compare.builtInName.equals("simulate")) {
							if (compare.builtinParameters.size() != 2) {
								System.err.println("builtin.simulate() need 2 parameters");
								break;
							}
							if (y < (Integer) listener.initStatements.get("simulator.startRow")) {
								continue;
							}
							int para1Offset = compare.builtinParameters.get(0).offset;
							int para2Offset = compare.builtinParameters.get(1).offset;
							if (y + para1Offset >= 0 && y + para2Offset >= 0) {
								int para1 = vcdTableModel.getColNo(compare.builtinParameters.get(0).module, compare.builtinParameters.get(0).wire);
								if (para1 == -1) {
									break;
								}
								appendVCDCheckLog("-".repeat(60));
								long pc = vcdTableModel.geBigIntegerAt(y + para1Offset, para1).longValue();
								appendVCDCheckLog("pc=" + pc);
								int para2 = vcdTableModel.getColNo(compare.builtinParameters.get(1).module, compare.builtinParameters.get(1).wire);
								if (para2 == -1) {
									break;
								}
								long inst = vcdTableModel.geBigIntegerAt(y + para2Offset, para2).longValue();
								appendVCDCheckLog("valueAtParameter=" + Long.toHexString(pc) + " = " + Long.toHexString(inst));

								int col1 = vcdTableModel.getColNo(compare.module1, compare.wire1);
								if (col1 == -1) {
									break;
								}

								Simulator simulator;
								if (simulators.get(col1) == null) {
									// init simulator
									simulator = new Simulator(null);
									simulator.initRegistersAndMemory();

//									byte temp[] = new byte[1000];
//									MemoryMap memoryMap = new MemoryMap("code", 0x400000, 1000, temp);
//									simulator.memory.memoryMaps.put(0l, memoryMap);
									// end init simulator
									simulators.put(col1, simulator);
								} else {
									simulator = simulators.get(col1);
								}

								simulator.memory.writeByte(pc, (byte) inst, true, false);
								simulator.memory.writeByte(pc + 1, (byte) (inst >> 8), true, false);
								simulator.memory.writeByte(pc + 2, (byte) (inst >> 16), true, false);
								simulator.memory.writeByte(pc + 3, (byte) (inst >> 24), true, false);

								String data = ("s\n").repeat(1);
								System.setIn(new ByteArrayInputStream(data.getBytes()));
								try {
									simulator.startSimulation();
								} catch (IOException ex) {
								}
								String register = null;
								if (compare.returnValue.type == 4) {
									int registerNo = Integer.parseInt(compare.returnValue.NUMBER().getText());
									register = Registers.regXMap32.get(registerNo);
								} else if (compare.returnValue.type == 3) {
//									compare.returnValue.IDENTIFIER(1)
									register = "t2";
								}
								long simulatorReturn = simulator.registers.get(register).getValue().longValue();

								long v = vcdTableModel.geBigIntegerAt(y + compare.offset1, col1).longValue();
								appendVCDCheckLog("our value=" + Long.toHexString(v) + ", simulatorReturn=" + Long.toHexString(simulatorReturn));
								if (v == simulatorReturn) {
									appendVCDCheckLog("correct " + col1 + "," + y);
									vcdTableCellRenderer.simulatorCorrect.add(Pair.of(col1, y));
									vcdTableCellRenderer.simulatorCorrectColor.put(Pair.of(col1, y), Color.decode(statement.color));
								} else {
									vcdTableCellRenderer.simulatorWrong.add(Pair.of(col1, y));
//									vcdTableCellRenderer.simulatorWrongColor.put(Pair.of(col1, y), Color.red);
									vcdTableCellRenderer.simulatorWrongColor.put(Pair.of(col1, y), Color.decode(statement.color));
									appendVCDCheckLog("wrong " + col1 + "," + y);
								}

								// add checkpoint
								addCheckPoint(Pair.of(col1, y), Pair.of(col1, y));
								addCheckPoint(Pair.of(col1, y), Pair.of(para1, y + para1Offset));
								addCheckPoint(Pair.of(col1, y), Pair.of(para2, y + para2Offset));
								// end add checkpoint
							}
						}
					}
				}
			}
			vcdCheckList.setModel(listModel);
			vcdTableModel.fireTableDataChanged();

		} catch (Exception ex) {
			Logger.getLogger(QuantrVCDComponent.class
					.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_checkButtonActionPerformed

	void appendVCDCheckLog(String str) {
		vcdCheckLogTextArea.setText(vcdCheckLogTextArea.getText() + "\n" + str);
	}

    private void refreshVCDCheckButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshVCDCheckButtonActionPerformed
		try {
			vcdCheckEditorPane.setText(IOUtils.toString(new FileReader(vcdCheckFile)));
		} catch (IOException ex) {
			Logger.getLogger(QuantrVCDComponent.class
					.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_refreshVCDCheckButtonActionPerformed

    private void saveVCDCheckFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveVCDCheckFileButtonActionPerformed
		try {
			IOUtils.write(vcdCheckEditorPane.getText(), new FileOutputStream(vcdCheckFile), "utf8");

		} catch (IOException ex) {
			Logger.getLogger(QuantrVCDComponent.class
					.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_saveVCDCheckFileButtonActionPerformed

    private void rowHeightTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rowHeightTextFieldKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			minColWidthButtonActionPerformed(null);
		}
    }//GEN-LAST:event_rowHeightTextFieldKeyPressed

    private void marginTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_marginTextFieldKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			minColWidthButtonActionPerformed(null);
		}
    }//GEN-LAST:event_marginTextFieldKeyPressed

    private void preButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preButtonActionPerformed
		try {
			noneButtonActionPerformed(null);
			MyVCDCheckListener listener = VCDCheck.check(IOUtils.toString(new FileReader(vcdCheckFile)));
			String pre = (String) listener.initStatements.get("ui.pre");
			DefaultMutableTreeNode root = (DefaultMutableTreeNode) jTree1.getModel().getRoot();
			selectAllScopes(root, StringUtils.stripAll(pre.split(",")));
			updateJTreeAfterSelect(null);
		} catch (IOException ex) {
			Logger.getLogger(QuantrVCDComponent.class
					.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_preButtonActionPerformed

    private void fontSizeTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fontSizeTextFieldKeyPressed
		try {
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				vcdTableCellRenderer.setFontSize(Integer.parseInt(fontSizeTextField.getText()));
				vcdTableModel.fireTableStructureChanged();
				minColWidthButtonActionPerformed(null);
			}
		} catch (NumberFormatException ex) {
		}
    }//GEN-LAST:event_fontSizeTextFieldKeyPressed

    private void noOfRowTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_noOfRowTextFieldKeyPressed
		try {
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				vcdTableModel.noOfRow = Integer.parseInt(noOfRowTextField.getText());
				vcdTableModel.fireTableStructureChanged();
				minColWidthButtonActionPerformed(null);
			}
		} catch (NumberFormatException ex) {
		}
    }//GEN-LAST:event_noOfRowTextFieldKeyPressed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
		JFileChooser fileChooser = new JFileChooser();
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			try {
				File selectedFile = fileChooser.getSelectedFile();
				loadVCD(selectedFile);
			} catch (IOException ex) {
				Logger.getLogger(QuantrVCDComponent.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
    }//GEN-LAST:event_openButtonActionPerformed

    private void fontComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontComboBoxActionPerformed
		Font font = (Font) fontComboBox.getSelectedItem();
		if (font != null) {
			vcdTableCellRenderer.setFont(font.deriveFont(Font.PLAIN, 16));
			fontComboBox.setFont(font.deriveFont(Font.PLAIN, 16));
			vcdTable.repaint();
		}
    }//GEN-LAST:event_fontComboBoxActionPerformed

    private void signedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signedCheckBoxActionPerformed
		vcdTableModel.signed = signedCheckBox.isSelected();
		vcdTableModel.clearCache();
		vcdTableModel.fireTableDataChanged();
    }//GEN-LAST:event_signedCheckBoxActionPerformed

    private void hexRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hexRadioButtonActionPerformed
		vcdTableModel.type = "hex";
		vcdTableModel.clearCache();
		vcdTableModel.fireTableDataChanged();
    }//GEN-LAST:event_hexRadioButtonActionPerformed

    private void decRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decRadioButtonActionPerformed
		vcdTableModel.type = "dec";
		vcdTableModel.clearCache();
		vcdTableModel.fireTableDataChanged();
    }//GEN-LAST:event_decRadioButtonActionPerformed

    private void lafComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_lafComboBoxItemStateChanged
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			String temp = (String) lafComboBox.getSelectedItem();
			if (temp.equals("Orange")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/arc-theme-orange.theme.json"));
			} else if (temp.equals("Orange Dark")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/arc_theme_dark_orange.theme.json"));
			} else if (temp.equals("Dark")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/arc_theme_dark.theme.json"));
			} else if (temp.equals("Material Darker Contrast")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/Material Darker Contrast.theme.json"));
			} else if (temp.equals("Cyan")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/Cyan.theme.json"));
			} else if (temp.equals("Dark Purple")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/DarkPurple.theme.json"));
			} else if (temp.equals("Material Deep Ocean Contrast")) {
				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/Material Deep Ocean Contrast.theme.json"));
			}
			FlatLaf.updateUI();
			vcdTable.getTableHeader().setUI(myTableHeaderUI);
		}
    }//GEN-LAST:event_lafComboBoxItemStateChanged

    private void settingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingButtonActionPerformed
		SettingDialog d = new SettingDialog(null, true);
		d.setLocationRelativeTo(null);
		d.setVisible(true);
    }//GEN-LAST:event_settingButtonActionPerformed

	private void selectTree(boolean b) {
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) jTree1.getModel().getRoot();
		CheckBoxNode c = (CheckBoxNode) root.getUserObject();
		c.selected = b;
		selectAllSubNodes(root, b, null);
		jTree1.setSelectionRow(0);
		jTree1MouseClicked(null);
		jTree1.updateUI();
	}

	private void getAllSelectedNode(DefaultMutableTreeNode node, ArrayList<DefaultMutableTreeNode> selectedNodes) {
		CheckBoxNode checkboxNode = (CheckBoxNode) node.getUserObject();
		if (checkboxNode.selected) {
			selectedNodes.add(node);
		}
		for (int x = 0; x < node.getChildCount(); x++) {
			getAllSelectedNode((DefaultMutableTreeNode) node.getChildAt(x), selectedNodes);
		}
	}

	public static void expandAll(JTree tree, boolean expand) {
		expandAll(tree, expand, -1);
	}

	public static void expandAll(JTree tree, boolean expand, int maxLevel) {
		TreeNode root = (TreeNode) tree.getModel().getRoot();
		if (root != null) {
			expandAll(tree, new TreePath(root), expand, maxLevel, 0);
		}
	}

	private static void expandAll(JTree tree, TreePath treePath, boolean expand, int maxLevel, int currentLevel) {
		if (maxLevel != -1 && currentLevel >= maxLevel - 1) {
			return;
		}

		TreeNode node = (TreeNode) treePath.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			for (Enumeration e = node.children(); e.hasMoreElements();) {
				TreeNode n = (TreeNode) e.nextElement();
				DefaultMutableTreeNode t = (DefaultMutableTreeNode) n;
				CheckBoxNode checkboxNode = (CheckBoxNode) t.getUserObject();
				if (checkboxNode.userObject instanceof Scope) {
					Scope scope = (Scope) checkboxNode.userObject;
					TreePath path = treePath.pathByAddingChild(n);
					if (!scope.scopes.isEmpty()) {
						expandAll(tree, path, expand, maxLevel, currentLevel + 1);
					} else {
						expandAll(tree, path, false, maxLevel, currentLevel + 1);
					}
				}
			}
		}

		if (expand) {
			tree.expandPath(treePath);
		} else {
			tree.collapsePath(treePath);
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) throws UnsupportedLookAndFeelException, ParseException, IOException, ClassNotFoundException {
//		if (!System.getProperty("os.name").equals("Mac OS X")) {
//			try {
		System.setProperty("sun.java2d.uiScale", String.valueOf(Setting.getInstance().scale));
		UIManager.setLookAndFeel(new FlatLightLaf());

//				UIManager.setLookAndFeel(new FlatArcOrangeIJTheme());
		IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/arc-theme-orange.theme.json"));
//				IntelliJTheme.setup(QuantrVCDComponent.class.getResourceAsStream("/hk/quantr/vcdcomponent/theme/arc-theme-orange.theme.json"));

//				FlatLaf.setGlobalExtraDefaults(Collections.singletonMap("@accentColor", "#ff0000"));
//				Class<? extends LookAndFeel> lafClass = UIManager.getLookAndFeel().getClass();
//				FlatLaf.setup(lafClass.newInstance());
//				FlatLaf.updateUI();
//				Font font = UIManager.getLookAndFeelDefaults().getFont("defaultFont");
//				UIManager.getLookAndFeelDefaults().put("defaultFont", font.deriveFont(Font.PLAIN, 16));
//			} catch (Exception ex) {
//				System.err.println("Failed to initialize LaF");
//			}
//		}
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help", false, "help");
		options.addOption(Option.builder("f")
				.required(false)
				.hasArg()
				.argName("file")
				.desc("vcd file")
				.longOpt("vcd")
				.build());
		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar assembler-xx.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);

			} catch (java.text.ParseException ex) {
				Logger.getLogger(QuantrVCDComponent.class
						.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);

		JFrame frame = new JFrame();
		QuantrVCDComponent quantrVCDComponent = new QuantrVCDComponent();
		frame.setSize(1400, 800);
		frame.setIconImage(new ImageIcon(Class.forName("hk.quantr.vcdcomponent.QuantrVCDComponent").getResource("/hk/quantr/vcdcomponent/All Q Logos_purple Small.png")).getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(quantrVCDComponent, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Quantr VCD Component");
		frame.setVisible(true);

		if (cmd.hasOption("f")) {
			if (new File(cmd.getOptionValue("f")).exists()) {
				frame.setTitle(cmd.getOptionValue("f"));
				quantrVCDComponent.loadVCD(new File(cmd.getOptionValue("f")));
			} else {
				quantrVCDComponent.jTree1.setModel(null);
				((CardLayout) quantrVCDComponent.getLayout()).show(quantrVCDComponent, "main");
			}
		} else {
			quantrVCDComponent.jTree1.setModel(null);
			((CardLayout) quantrVCDComponent.getLayout()).show(quantrVCDComponent, "main");
		}
	}

	public void loadVCD(File file) throws FileNotFoundException, IOException {
		if (!file.exists()) {
			return;
		}
		this.file = file;
		String temp = IOUtils.toString(new FileInputStream(file), "utf-8");
		loadVCD(temp);
	}

	public void loadVCD(String str) throws FileNotFoundException, IOException {
		this.vcdStr = str;
		if (file == null) {
			loadVCD(null, str, null);
		} else {
			File tempFile = new File(file.getAbsoluteFile().getParent() + File.separator + file.getName().substring(0, file.getName().indexOf(".")) + ".vc");
			loadVCD(file.getAbsoluteFile().getParentFile(), str, tempFile);
		}
	}

	public void loadVCD(File folder, String fileContent, File vcdCheckFile) {
		this.folder = folder;
		this.fileContent = fileContent;
		this.vcdCheckFile = vcdCheckFile;
		QuantrVCDComponent parent = this;
		new Thread() {
			@Override
			public void run() {
				jProgressBar1.setString("Loading VCD");
				jsonEditorPane.setText(fileContent);
				jsonEditorPane.setCaretPosition(0);
				VCD vcd = QuantrVCDLibrary.convertVCDToObject(folder, fileContent, false);
				if (vcd.scope == null) {
					return;
				}
//			DefaultMutableTreeNode root = new DefaultMutableTreeNode(vcd.scope);
//			init(root, vcd.scope.scopes);

				DefaultMutableTreeNode root = new DefaultMutableTreeNode(new CheckBoxNode(vcd.scope, false));
				initJTreeSimulator(root);
				initJTree(root, vcd.scope.scopes);
				jTree1.setModel(new DefaultTreeModel(root));
				jTree1.setCellEditor(new CheckBoxNodeEditor(jTree1, parent));
				jTree1.setEditable(true);

//				jTree1.revalidate();
//				jTree1.repaint();
				vcdTableModel.init(vcd);

				((CardLayout) parent.getLayout()).show(parent, "main");
				parent.remove(progressPanel);
				expandAllScopesButtonActionPerformed(null);
				riseEdgeCheckBoxActionPerformed(null);
				allButtonActionPerformed(null);

				if (vcdCheckFile != null && vcdCheckFile.exists() && vcdCheckFile.isFile() && vcdCheckFile.canRead()) {
					checkButton.setVisible(true);
					refreshVCDCheckButtonActionPerformed(null);
				}

				minColWidthButtonActionPerformed(null);
			}
		}.start();
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton allButton;
    private javax.swing.JButton checkButton;
    private javax.swing.JButton clearButton;
    private javax.swing.JRadioButton decRadioButton;
    private javax.swing.JCheckBox exactMatchCheckBox;
    private javax.swing.JButton expandAllButton;
    private javax.swing.JButton expandAllScopesButton;
    private javax.swing.JComboBox<Font> fontComboBox;
    private javax.swing.JTextField fontSizeTextField;
    private javax.swing.JCheckBox fullWireCheckBox;
    private javax.swing.ButtonGroup hexDecbuttonGroup;
    private javax.swing.JRadioButton hexRadioButton;
    private javax.swing.JCheckBox highlightRiseEdgeCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JToolBar jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JTree jTree1;
    private javax.swing.JEditorPane jsonEditorPane;
    private javax.swing.JComboBox<String> lafComboBox;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextField marginTextField;
    private javax.swing.JButton maxScreenButton;
    private javax.swing.JButton minColWidthButton;
    private javax.swing.JTextField noOfRowTextField;
    private javax.swing.JButton noneButton;
    private javax.swing.JButton openButton;
    private javax.swing.JButton preButton;
    private javax.swing.JPanel progressPanel;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton refreshVCDCheckButton;
    private javax.swing.JCheckBox riseEdgeCheckBox;
    private javax.swing.JTextField rowHeightTextField;
    private javax.swing.JButton saveVCDCheckFileButton;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JCheckBox selectInOutOnlyCheckBox;
    private javax.swing.JCheckBox selectModuleCheckBox;
    private javax.swing.JCheckBox selectMultipleCheckBox;
    private javax.swing.JButton settingButton;
    private javax.swing.JCheckBox signedCheckBox;
    private javax.swing.JEditorPane vcdCheckEditorPane;
    private javax.swing.JList<Statement> vcdCheckList;
    private javax.swing.JTextArea vcdCheckLogTextArea;
    private javax.swing.JPanel vcdCheckPanel;
    private javax.swing.JToolBar vcdCheckToolBar;
    private javax.swing.JTable vcdTable;
    private javax.swing.JScrollPane vcdTableScrollPane;
    // End of variables declaration//GEN-END:variables

	private void initJTreeSimulator(DefaultMutableTreeNode node) {
		DefaultMutableTreeNode simulatorNode = new DefaultMutableTreeNode(new CheckBoxNode("Simulator", false));
		node.add(simulatorNode);

		simulatorNode.add(new DefaultMutableTreeNode(new CheckBoxNode("Delta", false)));
		simulatorNode.add(new DefaultMutableTreeNode(new CheckBoxNode("Register", false)));
		simulatorNode.add(new DefaultMutableTreeNode(new CheckBoxNode("Memory", false)));
	}

	private void initJTree(DefaultMutableTreeNode node, ArrayList<Scope> scopes) {
		jProgressBar1.setMaximum(scopes.size());
		int x = 1;
		if (modulesOrder != null) {
			List temp = Arrays.asList(modulesOrder);
			Collections.sort(scopes, new Comparator<Scope>() {
				@Override
				public int compare(Scope o1, Scope o2) {
					if (temp.contains(o1.name) && temp.contains(o2)) {
						return temp.indexOf(o1) - temp.indexOf(o2);
					} else if (temp.contains(o1.name) && !temp.contains(o2)) {
						return -1;
					} else if (!temp.contains(o1.name) && temp.contains(o2)) {
						return 1;
					} else {
						return o1.name.compareTo(o2.name);
					}
				}
			});
		}

		scopes.sort(new Comparator<Scope>() {
			@Override
			public int compare(Scope a, Scope b) {
				if (moduleOrder.indexOf(a.name) != -1 && moduleOrder.indexOf(b.name) != -1) {
					return Integer.valueOf(moduleOrder.indexOf(a.name)).compareTo(moduleOrder.indexOf(b.name));
				} else if (moduleOrder.indexOf(a.name) != -1 && moduleOrder.indexOf(b.name) == -1) {
					return -1;
				} else if (moduleOrder.indexOf(a.name) == -1 && moduleOrder.indexOf(b.name) != -1) {
					return 1;
				} else {
					return a.name.compareTo(b.name);
				}
			}
		});

		for (Scope scope : scopes) {
			jProgressBar1.setValue(x);
			jProgressBar1.setString(scope.name);
			DefaultMutableTreeNode child = new DefaultMutableTreeNode(new CheckBoxNode(scope, false));
			node.add(child);
			initJTree(child, scope.scopes);

			if (scope.scopes.isEmpty()) {
				scope.wires.sort(new Comparator<Wire>() {
					@Override
					public int compare(Wire a, Wire b) {
						try {
//						String pattern = ".*\\((\\d+)\\)";
							String pattern = ".*(\\[(\\d+)\\]|\\((\\d+)\\))";
							if (Pattern.matches(pattern, a.name) && Pattern.matches(pattern, b.name)) {
								Pattern r = Pattern.compile(pattern);
								Matcher m1 = r.matcher(a.name);
								Matcher m2 = r.matcher(b.name);
								m1.find();
								m2.find();
								Integer i1 = Integer.valueOf(m1.group(2));
								Integer i2 = Integer.valueOf(m2.group(2));
								return i1.compareTo(i2);
							} else {
								return a.name.compareTo(b.name);
							}
						} catch (NumberFormatException ex) {
							return a.toString().compareTo(b.toString());
						}
					}
				});

				for (Wire wire : scope.wires) {
					if (wire.type != null && wire.type.equals("input")) {
						DefaultMutableTreeNode w = new DefaultMutableTreeNode(new CheckBoxNode(wire, false));
						child.add(w);
					}
				}
				for (Wire wire : scope.wires) {
					if (wire.type != null && wire.type.equals("output")) {
						DefaultMutableTreeNode w = new DefaultMutableTreeNode(new CheckBoxNode(wire, false));
						child.add(w);
					}
				}
				for (Wire wire : scope.wires) {
					if (wire.type == null || (!wire.type.equals("input") && !wire.type.equals("output"))) {
						DefaultMutableTreeNode w = new DefaultMutableTreeNode(new CheckBoxNode(wire, false));
						child.add(w);
					}
				}
			}
			x++;
		}
	}

	JButton undoButton = new JButton("Undo");
	JButton redoButton = new JButton("Redo");
	UndoManager undoManager = new UndoManager();

	private void initUndoManager() {
		undoButton.setEnabled(false);
		redoButton.setEnabled(false);

		vcdCheckEditorPane.getDocument().addUndoableEditListener(new UndoableEditListenerImpl());

		undoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					undoManager.undo();
				} catch (CannotRedoException cre) {
					cre.printStackTrace();
				}
				updateButtons();
			}
		});

		redoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					undoManager.redo();
				} catch (CannotRedoException cre) {
					cre.printStackTrace();
				}
				updateButtons();
			}
		});
		vcdCheckEditorPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
		vcdCheckEditorPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Redo");
		ActionMap am = vcdCheckEditorPane.getActionMap();
		am.put("Undo", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (undoManager.canUndo()) {
						undoManager.undo();
					}
				} catch (CannotUndoException exp) {
					exp.printStackTrace();
				}
			}
		});
		am.put("Redo", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (undoManager.canRedo()) {
						undoManager.redo();
					}
				} catch (CannotUndoException exp) {
					exp.printStackTrace();
				}
			}
		});

		vcdCheckToolBar.add(undoButton);
		vcdCheckToolBar.add(redoButton);
	}

	public void updateButtons() {
		undoButton.setText(undoManager.getUndoPresentationName());
		redoButton.setText(undoManager.getRedoPresentationName());
		undoButton.setEnabled(undoManager.canUndo());
		redoButton.setEnabled(undoManager.canRedo());
	}

//	private ArrayList<Integer> getHightCol(ArrayList<Compare> compares) {
//		ArrayList<Integer> r = new ArrayList<>();
//		for (Compare compare : compares) {
//			if (compare.type == 0) {
//				if (compare.offset1 == 0) {
//					r.add(vcdTableModel.getColNo(compare.module1, compare.wire1));
//				}
//			} else if (compare.type == 1) {
//				if (compare.offset1 == 0) {
//					r.add(vcdTableModel.getColNo(compare.module1, compare.wire1));
//				} else if (compare.offset2 == 0) {
//					r.add(vcdTableModel.getColNo(compare.module2, compare.wire2));
//				}
//			}
//		}
//		return r;
//	}
	private void addCheckPoint(Pair<Integer, Integer> key, Pair<Integer, Integer> value) {
		if (vcdTableModel.checkpoints.get(key) == null) {
			ArrayList<Pair<Integer, Integer>> temp2 = new ArrayList<>();
			temp2.add(value);
			vcdTableModel.checkpoints.put(key, temp2);
		} else {
			vcdTableModel.checkpoints.get(key).add(value);
		}
	}

	public void updateVcdStr() {
		try {
			this.vcdStr = IOUtils.toString(this.file.toURI(), "utf-8");
		} catch (IOException ex) {
			Logger.getLogger(QuantrVCDComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private class UndoableEditListenerImpl implements UndoableEditListener {

		public UndoableEditListenerImpl() {
		}

		@Override
		public void undoableEditHappened(UndoableEditEvent e) {
			undoManager.addEdit(e.getEdit());
			updateButtons();
		}
	}

}
