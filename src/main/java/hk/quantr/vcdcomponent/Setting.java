package hk.quantr.vcdcomponent;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import org.apache.commons.io.IOUtils;

public class Setting {

	private static Setting setting = null;
	public int x;
	public int y;
	public int width;
	public int height;
	public int dividorLocation1;
	public int dividorLocation2;
	public int dividorLocation3;
	public float scale;
	public String saveProjectPath;
	public String openProjectPath;
	public Color gateColor;
	public Color wireColor;
	public Color gridColor;
	public String componentPath;
	public boolean windowMaximized;

	public Setting() {
		width = 800;
		height = 600;
		dividorLocation1 = 450;
		dividorLocation2 = 500;
		dividorLocation3 = 250;
		scale = 1;
		windowMaximized = false;
	}

	public static Setting getInstance() {
		if (setting == null) {
			setting = load();
		}
		return setting;
	}

	public static void main(String args[]) {
		Setting setting = Setting.getInstance();
		System.out.println(setting.x);
	}

	public void save() {
		XStream xstream = new XStream();
		xstream.alias("Setting", Setting.class);
		String xml = xstream.toXML(this);
		try {
			IOUtils.write(xml, new FileOutputStream(new File("settingVCDComponent.xml")), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Setting load() {
		try {
			XStream xstream = new XStream(new StaxDriver());
			xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
			xstream.allowTypesByWildcard(new String[]{"hk.quantr.logic.*"});
			xstream.alias("Setting", Setting.class);
			Setting setting = (Setting) xstream.fromXML(new File("settingVCDComponent.xml"));
			return setting;
		} catch (Exception ex) {
			new File("setting.xml").delete();
			setting = new Setting();
			setting.save();
			return setting;
		}
	}

}
