# Introduction

This project contains two parts, a visualized component of [VCD format](https://en.wikipedia.org/wiki/Value_change_dump) It is a JComponent which can use indenpently in any java swing project, it is also a supporting project for our [Netbeans verilog plugin](https://gitlab.com/quantr/toolchain/netbeans-verilog).

![](https://www.quantr.foundation/wp-content/uploads/2021/12/Screenshot-2021-12-25-at-12.53.29-AM.png)

![](https://www.quantr.foundation/wp-content/uploads/2021/11/Screenshot_8.png)


# Wiki

The grammar and compiler is in this project, built completely by java and antlr

# Author

Peter <peter@quantr.hk>, System Architect from Quantr Limited

